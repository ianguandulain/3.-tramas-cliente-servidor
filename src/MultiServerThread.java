
import java.net.*;
import java.io.*;
import java.lang.Math;
import java.util.Date;

public class MultiServerThread extends Thread {

    private Socket socket = null;

    public MultiServerThread(Socket socket) {
        super("MultiServerThread");
        this.socket = socket;
        ServerMultiClient.NoClients++;
    }

    public void run() {

        try {
            PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String lineIn, lineOut;

            while ((lineIn = entrada.readLine()) != null) {
                System.out.println("Received: " + lineIn);
                escritor.flush();
                if (lineIn.equals("FIN")) {
                    ServerMultiClient.NoClients--;
                    break;
                } else {
                    char[] ch = new char[lineIn.length()];
                    for (int i = 0; i < lineIn.length(); i++) {
                        ch[i] = lineIn.charAt(i);
                    }
                    if (ch[0] == '#') {
                        String comando = String.valueOf(ch[1]) + String.valueOf(ch[2]) + String.valueOf(ch[3]);
                        String cadena = "";
                        for (int i = 7; i < lineIn.length() - 1; i++) {
                            cadena = cadena + String.valueOf(ch[i]);
                        }
                        String retorno = "";
                        switch (comando) {
                            case "may":
                                retorno = cadena.toUpperCase();
                                break;
                            case "min":
                                retorno = cadena.toLowerCase();
                                break;
                            case "inv":
                                for (int i = cadena.length() - 1; i >= 0; i--) {
                                    retorno = retorno + cadena.charAt(i);
                                }
                                break;
                            case "len":
                                retorno = Integer.toString(cadena.length());
                                break;
                            case "alea":
                                int rango = Integer.parseInt(cadena) - 0 + 1;
                                retorno = Integer.toString((int) (Math.random() * rango) + 0);
                                break;
                            default:
                                escritor.println("El comando _" + comando + "_ no se reconoce.");
                                break;
                        }
                        String rtrn = "";
                        for (int i = 0; i < 7; i++) {
                            rtrn = rtrn + String.valueOf(ch[i]);
                        }
                        rtrn = rtrn + retorno + "#";
                        escritor.println(rtrn);
                    } else {
                        escritor.println("La petición _" + lineIn + "_ no se reconoce.");
                    }
                    escritor.flush();
                }
            }
            try {
                entrada.close();
                escritor.close();
                socket.close();
            } catch (Exception e) {
                System.out.println("Error : " + e.toString());
                socket.close();
                System.exit(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
